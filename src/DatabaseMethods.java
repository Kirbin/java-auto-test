import org.openqa.selenium.WebDriver;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DatabaseMethods {

    private static final String username = "root";
    private static final String password = "root";
    private static final String connectionUrl = "jdbc:mysql://localhost:3306/javatest?autoReconnect=true&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&characterEncoding=utf8";

    private static Connection con;
    private static Statement stmt;
    private static ResultSet rs;

    public static void LoadIntoDatabase(List<Item> items){

        try
        {
            con = DriverManager.getConnection(connectionUrl, username, password);
            System.out.println("Connected!");
            stmt = con.createStatement();
            String query = "DELETE FROM javatest.buylist;";
            stmt.executeUpdate(query);
            for(Item item : items){
                query = "INSERT INTO javatest.buylist (name, count, price) \n" +
                        " VALUES (\'"+item.name+"\', "+item.count+", "+item.price+");";
                stmt.executeUpdate(query);
            }

        }

        catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }

        finally {
            try { con.close(); } catch(SQLException se) {}
            try { stmt.close(); } catch(SQLException se) {}
        }
    }

    public static List<Item> GetFromDatabase(){

        List<Item> items = new ArrayList<Item>();
        try
        {
            con = DriverManager.getConnection(connectionUrl, username, password);
            System.out.println("Connected!");
            stmt = con.createStatement();
            rs = stmt.executeQuery("select * from buylist");

            while (rs.next()) {
                String name = rs.getString(2);
                int count = rs.getInt(3);
                int price = rs.getInt(4);
                items.add(new Item(name, count, price));
            }

        }

        catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }

        finally {
            try { con.close(); } catch(SQLException se) {}
            try { stmt.close(); } catch(SQLException se) {}
            try { rs.close(); } catch(SQLException se) {}
        }
        return items;
    }

}
