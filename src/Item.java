public class Item{

    public String name;
    public String getName() {
        return name;
    }
    public void setName (String Name) {
        name = Name;
    }

    public int count;
    public int getCount() {
        return count;
    }
    public void setName (int Count) {
        count = Count;
    }

    public int price;
    public int getPrice() {
        return price;
    }
    public void setPrice (int Price) {
        price = Price;
    }

    public Item(String name, int count, int price){
        this.name = name;
        this.count = count;
        this.price = price;
    }
}
