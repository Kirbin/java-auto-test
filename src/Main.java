import org.openqa.selenium.WebDriver;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {


    public static void main(String[] args) {

        WebTableMethods.Setup();
        List<Item> webTableData = WebTableMethods.GetTableData();

        DatabaseMethods.LoadIntoDatabase(webTableData);

        WebTableMethods.DeleteTableData();

        List<Item> newItems = new ArrayList<Item>();
        newItems.add(new Item("Колдовской шар", 25, 100));
        newItems.add(new Item("Пиратский флаг", 1, 105));
        newItems.add(new Item("Магический посох", 2, 125));

        WebTableMethods.AddTableData(newItems);



        List<Item> databaseItems = DatabaseMethods.GetFromDatabase();
        List<Item> webTableItems = WebTableMethods.GetTableData();

            for(int i = 0; i < webTableItems.size(); i++){
                try{
                    if (webTableItems.get(i).name.compareTo(databaseItems.get(i).name) != 0){
                        System.out.println("Предмет \""+databaseItems.get(i).name+"\" изменен на \""+webTableItems.get(i).name+"\".");
                    }
                    if(webTableItems.get(i).count != databaseItems.get(i).count){
                        System.out.println("Количество \""+webTableItems.get(i).name+"\" изменилось с "+
                                                           databaseItems.get(i).count+" на "+
                                                           webTableItems.get(i).count+".");
                    }
                    if(webTableItems.get(i).price != databaseItems.get(i).price){
                        System.out.println("Цена \""+webTableItems.get(i).name+"\" изменилась с "+
                                                     databaseItems.get(i).price+" на "+
                                                     webTableItems.get(i).price+".");
                    }

                }
                catch(IndexOutOfBoundsException ex){
                    System.out.println("Добавлена новая строчка! \""+webTableItems.get(i).name+" "+
                            webTableItems.get(i).count+" "+
                            webTableItems.get(i).price+"\".");
                }

            }
       if(webTableItems.size() < databaseItems.size()){
                System.out.println("Удалено "+(databaseItems.size() - webTableItems.size())+" предметов!");
       }



        WebTableMethods.TearDown();
    }

}
