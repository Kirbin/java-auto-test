import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class WebTableMethods {
    private static WebDriver driver;

    public static void Setup() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.navigate().to("http://tereshkova.test.kavichki.com/");
    }

    public static List<Item> GetTableData() {
        List<WebElement> rows = driver.findElements(By.xpath("//table//tbody//tr"));
        List<Item> items = new ArrayList<Item>();
        for(WebElement row : rows){
            String name = row.findElement(By.xpath("./td[1]")).getText();
            int count = Integer.parseInt(row.findElement(By.xpath("./td[2]")).getText());
            int price = Integer.parseInt(row.findElement(By.xpath("./td[3]")).getText());
            items.add(new Item(name, count, price));

        }
        System.out.println("finished");
        return items;

    }

    public static void AddTableData(List<Item> items){

        driver.findElement(By.id("open")).click();

        for(Item item : items){
            WebElement newName = driver.findElement(By.id("name"));
            newName.clear();
            newName.sendKeys(item.name);

            WebElement newCount = driver.findElement(By.id("count"));
            newCount.clear();
            newCount.sendKeys(String.valueOf(item.count));

            WebElement newPrice = driver.findElement(By.id("price"));
            newPrice.clear();
            newPrice.sendKeys(String.valueOf(item.price));

            driver.findElement(By.id("add")).click();
        }
    }

    public static void DeleteTableData(){

        Random rnd = new Random();
        int count = rnd.nextInt(3);

        for(int i = 1; i <= count; i++){
            driver.findElement(By.xpath("//table//tbody//tr["+(rnd.nextInt(3)+1)+"]//td[4]//a")).click();
        }

    }

    public static void TearDown() {

        driver.quit();
    }
}
