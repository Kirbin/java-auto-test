package nick;

import org.apache.xpath.operations.Bool;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class UITests {
    private static WebDriver driver;

    @BeforeClass
    public static void setup() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.navigate().to("http://tereshkova.test.kavichki.com/");
    }

    @Test
    public void AddDeleteTest() {

        String addName = "asdfaw";
        String addCount = "23";
        String addPrice = "50";
        driver.findElement(By.id("open")).click();
        WebElement name = driver.findElement(By.id("name"));
        name.clear();
        name.sendKeys(addName);

        WebElement count = driver.findElement(By.id("count"));
        count.clear();
        count.sendKeys(addCount);

        WebElement price = driver.findElement(By.id("price"));
        price.clear();
        price.sendKeys(addPrice);

        driver.findElement(By.id("add")).click();

        List<WebElement> rows = driver.findElements(By.xpath("//table//tbody//tr"));
        WebElement addedRow = null;
        for(WebElement row : rows){
            String rowName = row.findElement(By.xpath("./td[1]")).getText();
            if(rowName.compareTo(addName) == 0){
                addedRow = row;
            }
        }
        assert(addedRow != null);


        addedRow.findElement(By.xpath("./td[4]/a")).click();
        rows = driver.findElements(By.xpath("//table//tbody//tr"));
        Boolean rowDeleted = true;
        for(WebElement row : rows){
            String rowName = row.findElement(By.xpath("./td[1]")).getText();
            if(rowName.compareTo(addName) == 0){
                rowDeleted = false;
            }
        }
        assert(rowDeleted);
        //ТЕСТ НЕ ПРОЙДЕН


    }


    @Test
    public void DeleteTest(){

        String deleteName = "Плащ черный";
        List<WebElement> rows = driver.findElements(By.xpath("//table//tbody//tr"));
        for(WebElement row : rows){
            String name = row.findElement(By.xpath("./td[1]")).getText();
            if(name.compareTo(deleteName) == 0){
                row.findElement(By.xpath("./td[4]/a")).click();
            }
        }

        Boolean rowDeleted = true;
        rows = driver.findElements(By.xpath("//table//tbody//tr"));
        for(WebElement row : rows){
            String name = row.findElement(By.xpath("./td[1]")).getText();
            if(name.compareTo(deleteName) == 0){
                rowDeleted = false;
            }
        }
        assert(rowDeleted);
        //ТЕСТ НЕ ПРОЙДЕН


    }

    @Test
    public void AddTest(){
        String addName = "Бластер";
        String addCount = "2";
        String addPrice = "500";
        driver.findElement(By.id("open")).click();
        WebElement name = driver.findElement(By.id("name"));
        name.clear();
        name.sendKeys(addName);

        WebElement count = driver.findElement(By.id("count"));
        count.clear();
        count.sendKeys(addCount);

        WebElement price = driver.findElement(By.id("price"));
        price.clear();
        price.sendKeys(addPrice);
        driver.findElement(By.id("add")).click();

        List<WebElement> rows = driver.findElements(By.xpath("//table//tbody//tr"));
        WebElement addedRow = null;
        for(WebElement row : rows){
            String rowName = row.findElement(By.xpath("./td[1]")).getText();
            if(rowName.compareTo(addName) == 0){
                addedRow = row;
            }
        }
        assert(addedRow != null);
        // ТЕСТ ПРОЙДЕН
    }


    @AfterClass
    public static void tearDown() {

        driver.quit();
    }
}
